@ECHO OFF
SET /p test_skip="Skip Tests? (note that new api.json will not be generated): y/n:   "

IF %test_skip%==y (SET opts=-DskipTests)
ELSE (SET opts="")

SET DEVELOPMENT_HOME=%CD%
@ECHO ON
cd %DEVELOPMENT_HOME%\oxus-crm\
call mvn clean install %opts%

cd %DEVELOPMENT_HOME%\oxus-loan\
call mvn clean install %opts%

cd %DEVELOPMENT_HOME%\fas\
call mvn clean install %opts%

cd %DEVELOPMENT_HOME%\risc\
call mvn clean install %opts%
cd %DEVELOPMENT_HOME%\dwh\
call mvn clean install %opts%

cd %DEVELOPMENT_HOME%

call docker-compose build

call docker-compose -f docker-compose.yml -f oxus-kafka/docker-compose.yml up -d