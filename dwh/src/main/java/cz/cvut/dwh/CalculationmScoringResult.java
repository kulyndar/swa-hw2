package cz.cvut.dwh;


import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "calculation_result")
public class CalculationmScoringResult {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name= "id")
    private Long id;

    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "result")
    @Enumerated
    private ScoringResult result;

    @Column(name = "calculation_date")
    private LocalDateTime calculationDate;

    public CalculationmScoringResult() {
    }
    public CalculationmScoringResult(ClientScoringCalculatedResource resource) {
        this.amount = resource.getLoanAmount();
        this.calculationDate = LocalDateTime.now();
        this.clientId = resource.getClientId();
        this.result = resource.getResult();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public ScoringResult getResult() {
        return result;
    }

    public void setResult(ScoringResult result) {
        this.result = result;
    }

    public LocalDateTime getCalculationDate() {
        return calculationDate;
    }

    public void setCalculationDate(LocalDateTime calculationDate) {
        this.calculationDate = calculationDate;
    }
}
