package cz.cvut.dwh;

import cz.cvut.dwh.ClientScoringCalculatedResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClientScoringCalculatedListener {

    @Autowired
    private CalculationRepository repository;

    @KafkaListener(
            topics = "CALCULATED_SCORING",
            groupId = "group_one",
            containerFactory = "kafkaListenerContainerFactory",
            autoStartup = "true")
    public void consumeClientCalculatedScoring(@Payload List<ClientScoringCalculatedResource> resources){
        // LOGGER.info("\n/******* Consume TEST-TOPIC Partition ---->>>>>>    ONE ********/\n"+containers);
        resources.forEach(res -> {
            repository.save(new CalculationmScoringResult(res));
        });
    }
}
