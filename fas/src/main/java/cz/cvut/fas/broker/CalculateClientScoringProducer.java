package cz.cvut.fas.broker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class CalculateClientScoringProducer {

    @Autowired
    private KafkaTemplate<String, CalculateClientScoringResource> kafkaTemplate;

    public void sendMessage(CalculateClientScoringResource containerMsg) {
        Message<CalculateClientScoringResource> message = MessageBuilder
                .withPayload(containerMsg)
                .setHeader(KafkaHeaders.TOPIC, "SCORING")
                .build();
        this.kafkaTemplate.send(message);
    }
}
