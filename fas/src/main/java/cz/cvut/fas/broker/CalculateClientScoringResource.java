package cz.cvut.fas.broker;

import cz.cvut.fas.LoanResource;

import java.math.BigDecimal;

public class CalculateClientScoringResource {
    private Long clientId;
    private BigDecimal loanAmount;
    private String callbackUrl;

    public CalculateClientScoringResource(LoanResource loanResource) {
        this.clientId = loanResource.getClientId();
        this.loanAmount = loanResource.getLoanAmount();
        this.callbackUrl = loanResource.getCallbackUrl();
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }
}
