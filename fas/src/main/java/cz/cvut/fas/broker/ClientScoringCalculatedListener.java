package cz.cvut.fas.broker;

import cz.cvut.fas.loan.ClientCalculatedLoanResource;
import cz.cvut.fas.loan.OxusLoanServiceConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClientScoringCalculatedListener {

    @Autowired
    private OxusLoanServiceConnector loanServiceConnector;

    @KafkaListener(
            topics = "CALCULATED_SCORING",
            groupId = "group_one",
            containerFactory = "kafkaListenerContainerFactory",
            autoStartup = "true")
    public void consumeClientScoringCalculated(@Payload List<ClientScoringCalculatedResource> resource){
       // LOGGER.info("\n/******* Consume TEST-TOPIC Partition ---->>>>>>    ONE ********/\n"+containers);
        resource.forEach(res -> {
            loanServiceConnector.callback(res.getCallbackUrl(), new ClientCalculatedLoanResource(res));
        });
    }
}
