package cz.cvut.fas.loan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class OxusLoanServiceConnector {

    private static final Logger LOGGER = Logger.getLogger("GENERAL");
    @Autowired
    private RestTemplate restTemplate;

    public void callback(String callbackUrl, ClientCalculatedLoanResource clientCalculatedLoanResource) {
        LOGGER.setLevel(Level.ALL);
        ResponseEntity<Void> response = restTemplate.postForEntity(callbackUrl, clientCalculatedLoanResource, Void.class);
        if (response.getStatusCode().equals(HttpStatus.OK)) {
            LOGGER.info("OK from loan service");
        } else {
            LOGGER.warning("NOK from loan service");
        }
    }
}
