package cz.cvut.fas.loan;

import cz.cvut.fas.broker.ClientScoringCalculatedResource;

import java.math.BigDecimal;

public class ClientCalculatedLoanResource {
    private Long clientId;
    private BigDecimal loanAmount;
    private ScoringResult result;


    public ClientCalculatedLoanResource(ClientScoringCalculatedResource clientScoringCalculatedResource) {
        this.clientId = clientScoringCalculatedResource.getClientId();
        this.loanAmount = clientScoringCalculatedResource.getLoanAmount();
        this.result = clientScoringCalculatedResource.getResult();
    }

    public ClientCalculatedLoanResource() {
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public ScoringResult getResult() {
        return result;
    }

    public void setResult(ScoringResult result) {
        this.result = result;
    }
}
