package cz.cvut.fas;

import cz.cvut.fas.broker.CalculateClientScoringProducer;
import cz.cvut.fas.broker.CalculateClientScoringResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/loan")
public class LoanController {

    @Autowired
    private CalculateClientScoringProducer producer;

    @GetMapping
    public ResponseEntity<Void> calculateClientScore(@RequestBody LoanResource loanResource) {
        producer.sendMessage(new CalculateClientScoringResource(loanResource));
        return ResponseEntity.ok().build();
    }

}
