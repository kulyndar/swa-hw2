# Run configuration 
## Docker
1. Run ```docker ps``` and copy kafka container name
2. Run ```docker build -t image_name --build-arg KAFKA=kafka_name_from_1:9092 .```
3. Run ```docker network ls``` and copy kafka network name
4. Run ```docker run -p 8080:8080 --network network_name_from_3 image_name```

## Docker compose
See ```docker-compose.yml``` file

To run whole docker-compose network including kafka, see ```build-and-run.bat```
## Without docker
Run as normal