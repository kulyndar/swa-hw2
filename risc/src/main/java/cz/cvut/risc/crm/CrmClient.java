package cz.cvut.risc.crm;

import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CrmClient {

    public ClientData getClientData(long clientId)
    {
        final String uri = "http://oxus-crm:8080/api/v1/client/" + clientId;
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<GetClientDataResponse> result = restTemplate.getForEntity(uri, GetClientDataResponse.class);

        System.out.println(result.getBody().getClientData());
        return result.getBody().getClientData();
    }
}
