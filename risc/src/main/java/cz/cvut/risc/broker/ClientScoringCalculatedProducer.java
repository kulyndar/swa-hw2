package cz.cvut.risc.broker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class ClientScoringCalculatedProducer {

    @Autowired
    private KafkaTemplate<String, ClientScoringCalculatedResource> kafkaTemplate;

    public void sendMessage(ClientScoringCalculatedResource containerMsg) {
        Message<ClientScoringCalculatedResource> message = MessageBuilder
                .withPayload(containerMsg)
                .setHeader(KafkaHeaders.TOPIC, "CALCULATED_SCORING")
                .build();
        this.kafkaTemplate.send(message);
    }
}
