package cz.cvut.risc.broker;

import java.math.BigDecimal;

public class ClientScoringCalculatedResource {
    private Long clientId;
    private BigDecimal loanAmount;
    private ScoringResult result;
    private String callbackUrl;


    public ClientScoringCalculatedResource(Long clientId, BigDecimal loanAmount, ScoringResult result, String callbackUrl) {
        this.clientId = clientId;
        this.loanAmount = loanAmount;
        this.result = result;
        this.callbackUrl = callbackUrl;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public ScoringResult isResult() {
        return result;
    }

    public void setResult(ScoringResult result) {
        this.result = result;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }
}
