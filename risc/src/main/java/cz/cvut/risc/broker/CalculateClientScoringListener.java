package cz.cvut.risc.broker;

import cz.cvut.risc.crm.ClientData;
import cz.cvut.risc.crm.CrmClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CalculateClientScoringListener {

    @Autowired
    private CrmClient crmClient;

    @Autowired
    private ClientScoringCalculatedProducer producer;

    @KafkaListener(
            topics = "SCORING",
            groupId = "group_one",
            containerFactory = "kafkaListenerContainerFactory",
            autoStartup = "true")
    public void consumeCalculateClientScoring(@Payload List<CalculateClientScoringResource> resources){
        resources.forEach(resource -> {
            ClientData client = crmClient.getClientData(resource.getClientId());
            ScoringResult result = (client.getIncome() - client.getExpense()) * 60 > resource.getLoanAmount().intValue() ? ScoringResult.OK : ScoringResult.NOK;

            producer.sendMessage(new ClientScoringCalculatedResource(
                    resource.getClientId(),
                    resource.getLoanAmount(),
                    result,
                    resource.getCallbackUrl()
            ));
        });
    }
}
